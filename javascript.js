// Data from Flickr
let apiKeyFlickr = "be955040f05c09baa89be29a41413dcc"
let secretKeyFlickr = "4ef6146c4be3aa50";
let urlAPIJSon = "";
let returnedObject = "";
let imgUrl = [];

// Get user location address
function geoFindMe() {

  const status = document.querySelector('#status');
  const mapLink = document.querySelector('#map-link');

  mapLink.href = '';
  mapLink.textContent = '';

  function success(position) {
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;

    status.textContent = '';
    mapLink.href = `https://www.openstreetmap.org/#map=18/${latitude}/${longitude}`;
    mapLink.textContent = `Latitude: ${latitude} °, Longitude: ${longitude} °`;
    changeUrl(latitude, longitude)
  }

  function error() {
    status.textContent = 'Unable to retrieve your location';
  }

  if (!navigator.geolocation) {
    status.textContent = 'Geolocation is not supported by your browser';
  } else {
    status.textContent = 'Locating…';
    navigator.geolocation.getCurrentPosition(success, error);
  }
}

document.querySelector('#find-me').addEventListener('click', geoFindMe);

function changeUrl(latitude, longitude) {
  urlAPIJSon = "https://www.flickr.com/services/rest/?api_key=" + apiKeyFlickr + "&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=" + latitude + "&lon=" + longitude + "&text=cachorros";
  hydrates(urlAPIJSon)
}

function hydrates(link) {
  fetch(link)
    .then(function (response) {
      return response.json();
    })
    .then(function (data) {
      returnedObject = data
      takeObjectsImgs(returnedObject)
    })
}

function takeObjectsImgs(returnedObject) {
  let arrayElements = []  
  returnedObject.photos.photo.forEach(element => {
    arrayElements.push(element)
  });
  createImgUrl(arrayElements)
}

function createImgUrl(arrayElements){
  let farm, server, id, secret;
  for (let i = 0; i < arrayElements.length; i++){
    farm = arrayElements[i].farm;
    server = arrayElements[i].server;
    secret = arrayElements[i].secret;
    id = arrayElements[i].id;
    imgUrl.push(`https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`)
  }
  timer();
}

let img = document.createElement('img');

function timer(){
  let count = 0;  
  let url = imgUrl[count]
  img.src= url;
  document.getElementById('imagem').appendChild(img)
  setInterval(function(){
    if (count === 3){
      count = 0;
    }
    let url = imgUrl[count]
    img.src= url;
    document.getElementById('imagem').appendChild(img)
    count += 1;
  }, 2000)
}
